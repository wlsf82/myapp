# My App

My first static Rails app based on the course Learn Ruby on Rails from Codecademy.

## Installation

Run `bundle install` to install the project dependencies.

## Starting the app

Run `rails server` to start the app locally.

### Accessing the app locally

After the rails server is started, access the following URL in your preferable web browser: http://localhost:3000/welcome

You should see something like the below sample.

![MyApp Rails](./my-app.png)